#include <pybind11/pybind11.h>

int add(int i, int j) {
    return i + j;
}

int sub(int i, int j) {
    return i - j;
}

PYBIND11_MODULE(toto, m) {
    m.def("add", &add, "toto add function");
    m.def("sub", &sub, "toto sub function");
}
