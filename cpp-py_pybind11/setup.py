from setuptools import setup, Extension


toto_module = [
    Extension(
        'toto',
        ['src/main.cpp']
    )
]


setup(
    name='toto',
    version='0.1.0',
    ext_modules=toto_module,
    install_requires=['pybind11>=2.2']
)
