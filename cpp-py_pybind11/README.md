
## linux + pip

- first install:

```
pip install .
```

- a quick test:

```
python -c "import toto; print(toto.add(22,20))"
```

- python REPL:

```
python 
```

- update:

```
pip install . --upgrade
```


## Nix

- a quick test:

```
nix-shell --run 'python -c "import toto; print(toto.add(22,20))"'
```

- python REPL:

```
nix-shell --run python
```

