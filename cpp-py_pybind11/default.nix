with import <nixpkgs> {};

let 

  _pybind11 = callPackage ./pybind11.nix { 
    inherit stdenv fetchFromGitHub cmake;
    python = python3;
  };

in 

pkgs.python3Packages.buildPythonPackage {
  name = "toto";
  src = ./.;
  buildInputs = [ _pybind11 ];
}
