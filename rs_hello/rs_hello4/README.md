# rs-hello4

The nix-way config (using carnix). For build and shell.

- generate config:

```
cargo update
carnix --standalone --src . --output build.nix Cargo.lock
```

- shell:

```
nix-shell build.nix -A rs_hello4_0_1_0
cargo run toto
```

- build:

```
nix-build build.nix -A rs_hello4_0_1_0
./result/bin/rs_hello4 toto
```

