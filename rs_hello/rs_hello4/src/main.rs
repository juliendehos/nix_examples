#[macro_use]
extern crate structopt;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Options {
    #[structopt(name = "name")]
    name: String,
}

fn main() {
    let command = Options::from_args();
    println!("hello4 {}", &command.name);
}

