with import <nixpkgs> {};
rustPlatform.buildRustPackage rec {
  name = "rs-hello1-${version}";
  version = "0.1.0";
  src = ./. ;
  buildInputs = [ pkgconfig openssl ];
  cargoSha256 = "19sc2i94w9pn4scwn3g6ic3023i6zqd8fv9j1csq4w3cc0829cwj";
}
