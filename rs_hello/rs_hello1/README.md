# rs-hello1

Builds everything. Not for coding.

- build and run :

```
nix-build
./result/bin/rs-hello1 toto
```

- if you modify the dependencies in `Cargo.toml`:
    - run `cargo update` to update `Cargo.lock` 
    - update `cargoSha256` in `default.nix` 

