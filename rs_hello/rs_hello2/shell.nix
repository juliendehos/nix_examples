with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "rs-hello2";
  buildInputs = [ rustc cargo pkgconfig openssl ];
  RUST_BACKTRACE = 1;
}

