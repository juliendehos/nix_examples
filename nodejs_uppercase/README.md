
- install node2nix :

```
nix-env -iA nixos.nodePackages.node2nix
```


- build nix package :

```
node2nix -6 -i package.json
```


- run in a shell :

```
nix-shell -A shell
node app.js
```

then <http://localhost:8080>

