# cpp_checkerboard

A Nix configuration for a C++ project (simple image processing) with package overriding.

## build environment

- see [default.nix](default.nix)

- build and run locally:

    ```
    $ nix-build
    $ ./result/bin/test_checkerboard 
    ```

[Back home](../README.md)

