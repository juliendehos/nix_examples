with import <nixpkgs> {};

let
    _opencv3gtk = opencv3.override { enableGtk2 = true; };

    _opencv3gtk_att = _opencv3gtk.overrideDerivation (attrs: {
        cmakeFlags = [attrs.cmakeFlags "-DENABLE_PRECOMPILED_HEADERS=OFF"];
    });
in

stdenv.mkDerivation {
    name = "checkerboard";
    src = ./.;
    buildInputs = [ cmake _opencv3gtk_att pkgconfig ];
}

