# hs-cabal_tictactoe

A Nix configuration for a Haskell + Cabal project (a simple tictactoe game with no AI).

## setup

- create a file `tictactoe.nix` from the cabal project configuration ([tictactoe.cabal](tictactoe.cabal)):

    ```
    $ cabal2nix . > tictactoe.nix
    ```

## build environment

- see [default.nix](default.nix)

- build project :

    ```
    $ nix-build
    ```

- run project :

    ```
    $ ./result/bin/tictactoe
    ```

## shell environment

- see [shell.nix](shell.nix)

- launch a nix environment:

    ```
    $ nix-shell
    ```

- launch cabal commands as usual:

    ```
    $ cabal build
    $ cabal run
    ```

[Back home](../README.md)

