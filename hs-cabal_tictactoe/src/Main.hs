import Data.Char
import Data.List
import Data.List.Split

type Board = String
type Player = Char
type Game = (Board, Player)
type Move = Int

newGame :: Game
newGame = (replicate 9 ' ', 'X')

showGame :: Game -> String
showGame (b, m) = "\n" ++ (showBoard b) ++ "\nPlayer " ++ [m]

showBoard :: Board -> String
showBoard b = intercalate "\n-+-+-\n" $ map (intersperse '|') bs
    where bs = chunksOf 3 b 

nextPlayer :: Player -> Player
nextPlayer 'X' = 'O'
nextPlayer 'O' = 'X'

play :: Game -> Move -> Game
play g@(b, p) m = if m < 1 || m > 9 || bm /= ' '
    then g 
    else (bh ++ [p] ++ bt, nextPlayer p)
        where bh = take (m - 1) b
              bt = drop m b
              bm = b !! (m - 1)

checkWinner :: Game -> Player
checkWinner (b, _) | checkWinnerPlayer b 'X' = 'X'
                   | checkWinnerPlayer b 'O' = 'O'
                   | otherwise = ' '

checkWinnerPlayer :: Board -> Player -> Bool
checkWinnerPlayer [a,b,c,d,e,f,g,h,i] p =
    [p,p,p] `elem` [[a,b,c],[d,e,f],[g,h,i],[a,d,g],
                    [b,e,h],[c,f,i],[a,e,i],[c,e,g]]

run :: Game -> IO ()
run g = do
    putStrLn $ showGame g
    let winner = checkWinner g
    if winner /= ' ' then 
        putStrLn $ winner : " wins"
    else do
        putStr "Enter move: "
        c <- getLine
        let mc = filter ((flip elem) "123456789") c
        if null mc then run g else run $ play g (read c :: Int)

main = run newGame

