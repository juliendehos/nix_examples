{
  # Don't handle stuff like the kernel, the bootloader, etc..
  boot.isContainer = true;

  # Just to have something to show eventually
  services.nginx.enable = true;
  system.activationScripts.createWelcomePage = ''
    mkdir -p /var/spool/nginx/html/
    echo "Hello from NixOS" > /var/spool/nginx/html/index.html
  '';
  networking.firewall.enable = false;
}
