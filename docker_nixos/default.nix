with import <nixpkgs> {};
let
  # Contains the definition for our nixos system
  nixos = import <nixpkgs/nixos> { configuration = ./configuration.nix; };

  # Patch the init script to run nginx instead of systemd
  wrappedSystem = symlinkJoin {

    name = "exec-systemd";

    paths = [ nixos.system ];

    postBuild = ''
      sed -i 's@exec systemd@exec ${nixos.config.systemd.services.nginx.runner}@' $out/init
    '';
  };
in

dockerTools.buildLayeredImage {
  name = "regnat/nixos";
  tag = "latest";

  contents = [wrappedSystem];
  config.Cmd = [ "/init" ];
}
