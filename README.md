# nix_examples

Example projects for developing with [Nix](https://nixos.org/nix/manual/).

## Introduction

Nix is a functional package manager. It can be used to develop, deploy and run
software environments that are reproducible, composable, isolated... 

Nix is the basis for:

- nixpkgs: a package collection 
- nixos: a linux distribution 
- nixops: a tool for deploying nixos machines
- hydra: a continuous integration tool
- disnix: a tool for deploying distributed services

See also:

- [About Nix](http://nixos.org/nix/about.html)
- [NixOS homepage](http://nixos.org/)
- [Nix Package Manager Guide](http://nixos.org/nix/manual)
- [Nixpkgs Contributors Guide](http://nixos.org/nixpkgs/manual)
- [Python on Nix](doc/python-on-nix.pdf)

## Table of contents

- [Nix installation](doc/installation.md)
- [Nix basics](doc/basics.md)
- [hs_tictactoe](hs_tictactoe/): ad-hoc Haskell example
- [hs-cabal_tictactoe](hs-cabal_tictactoe/): Haskell + Cabal example
- [py_matrix](py_matrix/): python script example
- [julia_myplot](julia_myplot/): julia script example
- [cpp_cvflip](cpp_cvflip/): C++ example
- [cpp_checkerboard](cpp_checkerboard/): C++ example with package overriding 
- [cpp-py_checkerboard](cpp-py_checkerboard/): C++ + Python/setuptools example

