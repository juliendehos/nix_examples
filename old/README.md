# nix configs for developers

## goals of these configs

- build and install project (`nix-env -f . -i...`)

- provide a development environment for building and running (`nix-shell ... ` then `nix-build ...` then `...`)

- build and run in a isolated environment, no installation (`nix-shell --pure --run ...`)


## C++ projects

| name            | description                                             |
|-----------------|---------------------------------------------------------|
| `myhello`       | simple C++/cmake project with no dependency             |
| `myhello_2`     | alternative packaging for myhello                       |
| `hello_glog`    | with some dependencies                                  |
| `checkerboard`  | with a customized dependency                            |
| `poly_fit`      | with a new package dependency                           |
| `mylib`         | library + executable                                    |


## Python projects

| name            | description                                               |
|-----------------|-----------------------------------------------------------|
| `hello_sin42`   | Python script with numpy dependency                       |
| `hello_sin42_2` | alternative packaging for hello_sin42                     |
| `plot_sin`      | with a customized dependency                              |
| `wordcount`     | with a new dependency                                     |
| `mod42`         | Pyhon module + test script                                |


## Mixed C++/Python projects

TODO

wordcount

mod42



C++ project + Python script 

C++ project + Python binding

