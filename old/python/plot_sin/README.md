# plot_sin

Python project with a customized dependency (matplotlib with tk support)

- installation:

```
nix-env -f . -iA plot_sin
plot_sin.py
```

- interactive session:

```
# interactive session
nix-shell --pure 
./plot_sin.py
```

- isolated run:

```
nix-shell --pure --run ./plot_sin.py
```

