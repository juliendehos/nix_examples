#!/usr/bin/env python3

import numpy as np
import matplotlib as mpl

mpl.use('TkAgg')

if __name__ == '__main__':

    import matplotlib.pyplot as plt

    X = np.arange(-5, 5, 0.1)
    Y = np.sin(X)

    fig = plt.figure()
    ax1 = fig.add_subplot(1, 1, 1)
    ax1.plot(X, Y)
    plt.show()
