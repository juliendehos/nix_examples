with import <nixpkgs> {}; 

let
    matplotlib_tk = (pkgs.python35Packages.matplotlib.override { 
        enableTk = true; 
    });
in

pkgs.stdenv.mkDerivation {
    name = "plot_sin";
    buildInputs = [ 
        matplotlib_tk 
        pkgs.stdenv pkgs.python3 pkgs.python35Packages.numpy 
    ];
}

