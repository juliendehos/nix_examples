# hello_sin42_2

Python project with numpy dependency
(alternative packaging)

- installation:

```
nix-env -f . -iA hello_sin42_2
hello_sin42_2.py
```

- interactive session:

```
# interactive session
nix-shell --pure 
hello_sin42_2.py
```

- isolated run:

```
nix-shell --pure --run hello_sin42_2.py
```

