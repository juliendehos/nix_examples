{ system ? builtins.currentSystem }:

let 
    pkgs = import <nixpkgs> { inherit system; };
in

pkgs.stdenv.mkDerivation rec {

    hello_sin42_2 = import ./hello_sin42_2.nix { 
        inherit (pkgs) stdenv python35Packages ; 
    };

    name = "run_hello_sin42_2";

    buildInputs = [ hello_sin42_2 pkgs.python35Packages.numpy ];
}

