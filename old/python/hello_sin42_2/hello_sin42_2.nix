{ stdenv, python35Packages }:

stdenv.mkDerivation {
    name = "hello_sin42_2";
    buildInputs = [ python35Packages.numpy ];
    src = ./.;
    installPhase = ''
        mkdir -p $out/bin
        cp hello_sin42_2.py $out/bin/
    '';
}

