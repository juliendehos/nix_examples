with import <nixpkgs> {}; 
with pkgs.python35Packages;

stdenv.mkDerivation {
    name = "hello_sin42";
    buildInputs = [ numpy ];
    src = ./.;
    installPhase = ''
        mkdir -p $out/bin
        cp hello_sin42.py $out/bin/
    '';
}

