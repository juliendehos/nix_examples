# hello_sin42

Python project with numpy dependency

- installation:

```
nix-env -f . -iA hello_sin42
hello_sin42.py
```

- interactive session:

```
# interactive session
nix-shell --pure 
./hello_sin42.py
```

- isolated run:

```
nix-shell --pure --run ./hello_sin42.py
```

