from setuptools import setup
setup(name='myapp',
      version='1.0.0',
      py_modules=['connection', 'server', 'worker'],
      install_requires=['aioamqp', 'tornado'])
