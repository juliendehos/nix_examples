# -*- coding: utf-8 -*-
import asyncio
import time
from pickle import loads
from connection import get_channel

counter = 0


async def work():
    channel = await get_channel()
    requests = (await channel.queue_declare('', exclusive=True))['queue']
    await channel.queue_bind(requests, 'amq.topic', '#')

    async def handle(body, envelope, properties):
        global counter
        counter += 1

        request = loads(body)
        response = request['uri']
        await channel.basic_publish(payload=response,
                                    exchange_name='',
                                    routing_key=properties.reply_to)
        print("{0:d} {1}: {2}".format(counter, time.ctime(), request['uri']))

    await channel.basic_consume(requests, no_ack=True, callback=handle)


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(work())
    asyncio.get_event_loop().run_forever()
