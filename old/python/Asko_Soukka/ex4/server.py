# -*- coding: utf-8 -*-
import asyncio
import tornado.platform.asyncio
import tornado.web
from pickle import dumps
from connection import get_channel


class MainHandler(tornado.web.RequestHandler):
    async def get(self):
        channel = await get_channel()
        reply_to = (await channel.queue_declare('', exclusive=True))['queue']
        routing_key = self.request.uri.strip('/').replace('/', '.') or '/'

        payload = dumps({key: getattr(self.request, key) for key in [
            'files', 'body_arguments', 'query', 'query_arguments',
            'body', 'path', 'method', 'uri', 'arguments',
            'cookies', 'host', 'headers', 'remote_ip'
        ]})
        await channel.publish(payload=payload,
                              exchange_name='amq.topic',
                              routing_key=routing_key,
                              properties={'reply_to': reply_to})

        queue = asyncio.Queue()
        async def handle(*args, messages=queue):
            await messages.put(args)

        consumer = (await channel.basic_consume(
            reply_to, no_ack=True, callback=handle)
        )['consumer_tag']

        body, envelope, properties = await queue.get()

        self.write(body)

        await channel.basic_cancel(consumer)
        await channel.queue_delete(reply_to)
        await channel.close()


app = tornado.web.Application([
    (r'.*', MainHandler),
])


if __name__ == "__main__":
    tornado.platform.asyncio.AsyncIOMainLoop().install()
    app.listen(8080)
    asyncio.get_event_loop().run_forever()
