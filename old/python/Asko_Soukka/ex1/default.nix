with import <nixpkgs> {};

let 

    toolz = pkgs.python35Packages.buildPythonPackage rec {
        name = "toolz-${version}";
        version = "0.8.0";
        src = pkgs.fetchurl {
            url = "mirror://pypi/t/toolz/toolz-${version}.tar.gz";
            sha256 = "e8451af61face57b7c5d09e71c0d27b8005f001ead56e9fdf470417e5cc6d479";
        };
        doCheck = false;
        };
    };

    _pytest = with pkgs.python35Packages; pytest.override rec {
        name = "pytest-${version}";
        version = "3.0.7";
        src = pkgs.fetchurl {
            url = "https://github.com/pytest-dev/pytest/archive/${version}.tar.gz";
            sha256 = "191jg7bz463xm43i4l3hbj4zc4mqkmxipx75j3kyjykln33qpgny";
        };
        propagatedBuildInputs = [ hypothesis py setuptools ];
        doCheck = false;
    };

in

stdenv.mkDerivation rec {
    name = "env";
    env = buildEnv { name = name; paths = buildInputs; };
    buildInputs = [ python35Packages.numpy _pytest ];
}

