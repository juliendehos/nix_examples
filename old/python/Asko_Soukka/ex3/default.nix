with import <nixpkgs> {};

let dependencies = rec {

  # Customized existing packages using expression override
  _tornado = with python35Packages; tornado.override rec {
    name = "tornado-4.3b1";
    src = fetchurl {
      url = "https://pypi.python.org/packages/source/t/tornado/${name}.tar.gz";
      sha256 = "c7ddda61d9469c5745f3ac00e480ede0703dd1a4ef540a3d9bd5e03e9796e430";
    };
  };

  # Custom new packages using buildPythonPackage expression
  _redis_structures= with python35Packages; buildPythonPackage rec {
    name = "redis_structures-0.1.3";
    src = fetchurl {
      url = "https://pypi.python.org/packages/source/r/redis_structures/${name}.tar.gz";
      sha256 = "4076cff3ea91b7852052d963bfd2533c74e8a0054826584e058e685a911f56c5";
    };
    # Fix broken packaging (package is missing README.rst)
    prePatch = "touch README.rst";
    # Define package requirements (without pythonPackages prefix)
    propagatedBuildInputs = [ redis ];
  };

};

in 

with dependencies;

stdenv.mkDerivation rec {

  name = "env";

  # Mandatory boilerplate for buildable env
  env = buildEnv { name = name; paths = buildInputs; };
  builder = builtins.toFile "builder.sh" ''
    source $stdenv/setup
    ln -s $env $out
  '';

  # Customizable development requirements
  buildInputs = [
    # Add packages from nix-env -qaP | grep -i needle queries
    redis

    # With Python configuration requiring a special wrapper
    (python35.buildEnv.override {
      ignoreCollisions = true;
      extraLibs = with python35Packages; [
        # Add pythonPackages without the prefix
        _tornado
        _redis_structures
      ];
    })
  ];

  # Customizable development shell setup with at last SSL certs set
  shellHook = ''
    export SSL_CERT_FILE=${cacert}/etc/ssl/certs/ca-bundle.crt
  '';

}

