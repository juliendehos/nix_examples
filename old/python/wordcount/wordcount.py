#!/usr/bin/env python3

def stem(word):
    ''' Stem word to primitive form '''
    return word.lower().rstrip(",.!:;'-\"").lstrip("'\"")

from toolz import compose, frequencies, partial
from toolz.curried import map

wordcount = compose(frequencies, map(stem), str.split)

if __name__ == '__main__':

    sentence = 'This cat jumped over this other cat!'
    result = wordcount(sentence)
    print(result)
