with import <nixpkgs> {};

( 
let
    mytoolz = pkgs.python35Packages.buildPythonPackage rec {
        name = "toolz-${version}";
        version = "0.8.2";
        src = pkgs.fetchurl{
            url = "mirror://pypi/t/toolz/toolz-${version}.tar.gz";
            sha256 = "0l3czks4xy37i8099waxk2fdz5g0k1dwys2mkhlxc0b0886cj4sa";
        };
        doCheck = false;
    };
in 

pkgs.python35.buildEnv.override rec {
    extraLibs = [ mytoolz ];
}
).env

