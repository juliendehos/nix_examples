# wordcount

Python project with a new dependency (toolz)

TODO

- installation:

```
nix-env -f . -iA wordcount
wordcount.py
```

- interactive session:

```
# interactive session
nix-shell --pure 
./wordcount.py
```

- isolated run:

```
nix-shell --pure --run ./wordcount.py
```

