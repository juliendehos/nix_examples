#!/bin/sh

POLY_FIT=poly_fit.out
#POLY_FIT=../build/poly_fit.out

# plot data 
gnuplot -e "set out 'out_data.png'; \
    set terminal png size 800,600; \
    set grid xtics ytics; \
    plot 'data_yoan.csv' title 'data' pt 5"

# compute fitting 
${POLY_FIT} data_yoan.csv 1 50 > out_fit1.csv
${POLY_FIT} data_yoan.csv 2 50 > out_fit2.csv

# plot fitting 
gnuplot -e "set out 'out_fit.png'; \
    set terminal png size 800,600; \
    set grid xtics ytics; \
    plot 'data_yoan.csv' title 'data' pt 5, \
         'out_fit1.csv' title 'linear' with line lw 2, \
         'out_fit2.csv' title 'quadratic' with line lw 2"

# display plots
eog out_data.png out_fit.png

