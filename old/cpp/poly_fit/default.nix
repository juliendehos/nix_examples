{ system ? builtins.currentSystem }:

let 
    pkgs = import <nixpkgs> { inherit system; };
    eigen = import ./nix/eigen.nix { inherit (pkgs) stdenv fetchurl cmake ; };
in

pkgs.stdenv.mkDerivation rec {

    name = "run_poly_fit";
    buildInputs = [ poly_fit pkgs.gnuplot pkgs.gnome3.eog ];

    poly_fit = import ./nix/poly_fit.nix {
        inherit eigen ;
        inherit (pkgs) stdenv cmake pkgconfig ;
    };

}

