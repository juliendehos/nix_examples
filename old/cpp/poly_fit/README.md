# poly_fit

C++/cmake project with a new package dependency (eigen-3.2.10)


- build and install:

```
nix-env -f . -iA poly_fit
poly_fit.out
```

- interactive session:

```
nix-shell 
nix-build -A poly_fit
poly_fit.out
cd run 
./run_poly_fit.sh
exit
```

- isolated run:

```
cd run
nix-shell --pure ../default.nix --run ./run_poly_fit.sh
```

