{ stdenv, cmake, pkgconfig, eigen }:

stdenv.mkDerivation {
    name = "poly_fit";
    buildInputs = [ cmake pkgconfig eigen ];
    src = ../. ;
}

