#include <Eigen/QR>
#include <Eigen/Dense>

#include <cassert>
#include <fstream>
#include <iostream>
#include <vector>

using namespace Eigen;

MatrixXd loadCsv(const std::string & path, const char sep) {

    // read input data linearly
    std::ifstream indata(path);
    std::string line;
    std::vector<double> values;
    unsigned rows = 0;
    while (std::getline(indata, line)) {
        std::stringstream lineStream(line);
        std::string cell;
        while (std::getline(lineStream, cell, sep)) {
            values.push_back(std::stod(cell));
        }
        ++rows;
    }

    // build matrix
    MatrixXd data = Map<MatrixXd>(values.data(), values.size()/rows, rows);
    data.transposeInPlace();
    return data;
}

VectorXd computePolyFit(const VectorXd & x, const VectorXd & y, int deg1) {

    // check inputs
	assert(x.rows() == y.rows());
	assert(x.rows() >= deg1);

    // create A
	Eigen::MatrixXd A = MatrixXd::Ones(x.rows(), deg1);
    for (int j=1; j<deg1; j++)
        A.col(j) = A.col(j-1).cwiseProduct(x);

    // solve Af=y for f
    return A.jacobiSvd(ComputeThinU|ComputeThinV).solve(y);
}

VectorXd evalPolyFit(const VectorXd & x, const VectorXd & f) {
    int deg1 = f.rows();
	Eigen::MatrixXd A = MatrixXd::Ones(x.rows(), deg1);
    for (int j=1; j<deg1; j++)
        A.col(j) = A.col(j-1).cwiseProduct(x);
    return A * f;
}

int main(int argc, char ** argv) {

    // get command line arguments
    if (argc != 4) {
        std::cout << "usage: " << argv[0] << " <filename> <deg> <divs>\n";
        exit(-1);
    }
    const std::string FILENAME = argv[1];
    const int DEG1 = 1 + atoi(argv[2]);
    const int DIVS = atoi(argv[3]);

    // load data
    MatrixXd data = loadCsv(FILENAME, ' ');
    VectorXd dataX = data.col(0);
    VectorXd dataY = data.col(1);
    std::clog << "\ndataX:\n" << dataX << std::endl;
    std::clog << "\ndataY:\n" << dataY << std::endl;

    // compute polynomial fitting
    VectorXd f = computePolyFit(dataX, dataY, DEG1);
    std::clog << "\nf:\n" << f << std::endl;

    // output some fitted data
    double minX = dataX.minCoeff();
    double maxX = dataX.maxCoeff();
    VectorXd fitX = VectorXd::LinSpaced(DIVS, minX, maxX);
    VectorXd fitY = evalPolyFit(fitX, f); 
    MatrixXd fit(DIVS, 2);
    fit << fitX, fitY;
    std::cout << fit << std::endl;

    return 0;
}

