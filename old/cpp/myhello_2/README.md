# myhello_2

simple C++/cmake project with no dependency 
(other packaging)

- build and install:

```
nix-env -f . -iA myhello_2
myhello_2
```

- interactive session:

```
nix-shell 
nix-build -A myhello_2
./result/bin/myhello_2
exit
```

- isolated run:

```
nix-shell --pure --run myhello_2
```

