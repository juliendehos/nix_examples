{ system ? builtins.currentSystem }:

let 
    pkgs = import <nixpkgs> { inherit system; };
in

pkgs.stdenv.mkDerivation rec {

    myhello_2 = import ./myhello_2.nix { inherit (pkgs) stdenv cmake ; };

    name = "run_myhello_2";

    buildInputs = [ myhello_2 ];
}

