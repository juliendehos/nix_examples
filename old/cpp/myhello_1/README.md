# myhello_1

simple C++/cmake project with no dependency 

- build and install:

```
nix-env -f . -i myhello_1
myhello_1
```

- interactive session:

```
nix-shell 
nix-build 
./result/bin/myhello_1
exit
```

- isolated run:

```
nix-build 
nix-shell --pure --run ./result/bin/myhello_1
```

