# hello_glog

C++/cmake project with some dependencies (libglog, pkg-config)


- build and install:

```
nix-env -f . -i hello_glog
hello_glog
```

- interactive session:

```
nix-shell 
nix-build 
./result/bin/hello_glog
exit
```

- isolated run:

```
nix-build 
nix-shell --pure --run ./result/bin/hello_glog
```

