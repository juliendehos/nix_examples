with import <nixpkgs> {}; 

stdenv.mkDerivation {
    name = "hello_glog";
    buildInputs = [ stdenv cmake pkgconfig glog ];
    src = ./.;
}

