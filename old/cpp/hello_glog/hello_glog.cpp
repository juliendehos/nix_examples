#include <iostream>
#include <glog/logging.h>

int main() {
    google::InitGoogleLogging("log_hello_glog");
    google::SetLogDestination(google::GLOG_INFO, "log_hello_glog");
    LOG(INFO) << "Hello world!";
    return 0;
}

