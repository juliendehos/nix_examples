{ stdenv, cmake, mylib }:

stdenv.mkDerivation {
    name = "main";
    src = ./.; 
    buildInputs = [ cmake mylib ];
}

