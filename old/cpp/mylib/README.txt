# mylib

C++/cmake project (library + executable)


- build and install:

```
nix-env -f . -i run_main
main.out
```

- interactive session:

```
nix-shell 
nix-build
main.out
exit
```

- isolated run:

```
nix-shell --pure --run main.out
```

- using cmake only (without nix) :

```
mkdir build_cmake
cd build_cmake
cmake ..
make
./main.out
```

