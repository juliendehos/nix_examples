{ system ? builtins.currentSystem }:

let 
    pkgs = import <nixpkgs> { inherit system; };
in

pkgs.stdenv.mkDerivation rec {

    name = "run_main";
    src = ./.;
    buildInputs = [ mylib main ];
    installPhase = ''
        mkdir $out
        echo "nothing to install (meta package)"
    '';

    mylib = import ./mylib/default.nix { inherit (pkgs) stdenv cmake; };

    main = import ./main/default.nix { 
        inherit mylib; 
        inherit (pkgs) stdenv cmake; 
    };


}

