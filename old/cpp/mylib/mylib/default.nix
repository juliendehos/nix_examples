{ stdenv, cmake }:

stdenv.mkDerivation {
    name = "mylib";
    src = ./.; 
    buildInputs = [ cmake ];
}

