with import <nixpkgs> {};

let
    opencv_gtk = (pkgs.opencv.override { 
        enableGtk2 = true; 
    }).overrideDerivation (attrs: {
        postPatch = (attrs.postPatch or "") + ''
            sed -i '/Add these standard paths to the search paths for FIND_LIBRARY/,/^\s*$/{d}' CMakeLists.txt
        '';
    });
in

pkgs.stdenv.mkDerivation {
    name = "checkerboard";
    buildInputs = [ pkgs.stdenv pkgs.cmake pkgs.pkgconfig opencv_gtk ];
    src = ./.;
}

