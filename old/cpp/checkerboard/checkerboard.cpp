#include <opencv2/opencv.hpp>

int main() {

    const unsigned WIDTH = 800;
    const unsigned HEIGHT = 600;

    cv::Mat img(HEIGHT, WIDTH, CV_8U);
    for (unsigned i=0; i<HEIGHT; i++) {
        for (unsigned j=0; j<WIDTH; j++) {
            const unsigned EVEN_I = (i/200) % 2;
            const unsigned EVEN_J = (j/200) % 2;
            img.at<unsigned char>(i, j) = EVEN_I == EVEN_J ? 255 : 0;
        }
    } 

    cv::imshow("checkerboard", img);

    while (true) {
        int key = cv::waitKey(30) % 0x100;
        if (key == 27) 
            break;
    }

    return 0;
}

