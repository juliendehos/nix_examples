# checkerboard

C++/cmake project with a customized dependency (opencv with gtk support)

- build and install:

```
nix-env -f . -i checkerboard
checkerboard
```

- interactive session:

```
nix-shell 
nix-build 
./result/bin/checkerboard
exit
```

- isolated run:

```
nix-build 
nix-shell --pure --run ./result/bin/checkerboard
```

