# julia_myplot

A Nix configuration for a simple julia script (plot a function).

**This example is experimental...**

## shell environment

- see [shell.nix](shell.nix)

- launch a nix environment and run the script:

    ```
    $ nix-shell 
    $ julia init.jl
    $ julia myplot.jl
    ```

[Back home](../README.md)



