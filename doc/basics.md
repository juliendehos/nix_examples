# Nix basics

- [Nix Package Manager Guide](https://nixos.org/nix/manual/#chap-package-management)

## Packages

- List installed packages:

    ```
    $ nix-env -q
    ```

- List available packages containing "firefox":

    ```
    $ nix-env -qaP '.*firefox.*'
    ```

- Install a package:

    ```
    $ nix-env -i firefox
    ```

- Install a package (faster):

    ```
    $ nix-env -iA nixpkgs.firefox
    ```

- remove a package:

    ```
    $ nix-env -e firefox
    ```


## Updates

- update channel:

    ```
    $ nix-channels --update
    ```

- update all packages:

    ```
    $ nix-env -u
    ```

- clean store:

    ```
    $ nix-collect-garbage -d
    ```


## Shell environment

- run a shell (with the `hello` package):

    ```
    $ nix-shell -p hello

    [nix-shell:~]$ hello
    Bonjour, le monde !

    [nix-shell:~]$ which hello
    /nix/store/fbvxff7alab12hfcgg475687p1dadf3h-hello-2.10/bin/hello
    ```


- run a shell in a pure environment:

    ```
    $ nix-shell --pure -p hello 

    [nix-shell:~]$ hello
    Hello, world!

    [nix-shell:~]$ which hello
    bash: which: command not found
    ```

- use a `shell.nix` file:

    ```
    with import <nixpkgs> {};
    stdenv.mkDerivation {
        name = "my-env";
        buildInputs = [ hello ];
    }
    ```



See also the [nix page from SHARCNET](https://www.sharcnet.ca/help/index.php/NIX).



## Generations

- list generations:

    ```
    $ nix-env --list-generations
      55   2017-08-15 12:50:46   (current)

    $ nix-env -i hello
    installing ‘hello-2.10’
    ...

    $ nix-env -q 
    hello-2.10

    $ nix-env --list-generations
      55   2017-08-15 12:50:46   
      56   2017-08-15 13:54:11   (current)

    $ nix-env -i nox tmux
    ...
    $ nix-env -i vim
    ...
    $ nix-env --list-generations
      55   2017-08-15 12:50:46   
      56   2017-08-15 13:54:11   
      57   2017-08-15 13:56:17   
      58   2017-08-15 13:56:24   (current)

    $ nix-env -q
    hello-2.10
    nox-0.0.5
    tmux-2.5
    vim-8.0.0442
    ```


- rollback:

    ```
    $ nix-env --rollback
    switching from generation 58 to 57

    $ nix-env --list-generations
      55   2017-08-15 12:50:46   
      56   2017-08-15 13:54:11   
      57   2017-08-15 13:56:17   (current)
      58   2017-08-15 13:56:24   

    $ nix-env -q
    hello-2.10
    nox-0.0.5
    tmux-2.5
    ```


- switch generation:

    ```
    $ nix-env --switch-generation 58
    switching from generation 57 to 58

    $ nix-env --list-generations
      55   2017-08-15 12:50:46   
      56   2017-08-15 13:54:11   
      57   2017-08-15 13:56:17   
      58   2017-08-15 13:56:24   (current)

    $ nix-env -q
    hello-2.10
    nox-0.0.5
    tmux-2.5
    vim-8.0.0442
    ```

- delete generations:

    ```
    $ nix-env --delete-generations 56 57
    removing generation 56
    removing generation 57

    $ nix-env --list-generations
      55   2017-08-15 12:50:46   
      58   2017-08-15 13:56:24   (current)

    $ nix-env -q
    hello-2.10
    nox-0.0.5
    tmux-2.5
    vim-8.0.0442

    $ nix-env --rollback
    switching from generation 58 to 55

    $ nix-env -q
    hello-2.10
    ```

[Back home](../README.md)

