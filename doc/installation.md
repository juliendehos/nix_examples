# Nix installation

## Debian

- install nix:

    ```
    $ sudo apt install curl
    $ curl https://nixos.org/nix/install | sh
    ```

- disconnect and relog, or:

    ```
    $ . $HOME/.nix-profile/etc/profile.d/nix.sh
    ```

## Void Linux

- install nix:

    ```
    $ sudo xbps-install nix xz
    $ . /etc/profile
    $ sudo ln -s /etc/sv/nix-daemon /var/service/
    ```

- add a channel:

    ```
    $ nix-channel --add https://nixos.org/channels/nixpkgs-unstable
    $ nix-channel --update
    ```

## Post-installation (optional)

- test installation (see [Nix basics](basics.md) or the [Nix manual](https://nixos.org/nix/manual/)):

    ```
    $ nix-env -qa nox
    nox-0.0.5
    $ nix-env -iA nixpkgs.nox
    $ nox hello
    1 iagno-3.22.0 (nixos.gnome3.iagno)
        Computer version of the game Reversi, more popularly called Othello
    2 hello-2.10 (nixos.hello)
        A program that produces a familiar, friendly greeting
    Packages to install: 2
    installing ‘hello-2.10’
    ...
    ```

- switch to a stable channel (see [nixos channels](https://nixos.org/channels/)):

    ```
    $ nix-channel --remove nixpkgs
    $ nix-channel --add https://nixos.org/channels/nixos-17.03 nixpkgs
    $ nix-channel --update
    $ nix-env -u
    ```

- software GL (in case of `libGL error: failed to load driver: swrast`):

    ```
    $ nix-env -i mesa-noglu
    $ sudo ln -s /nix/store/*-mesa-noglu-*-drivers /run/opengl-driver
    ```

[Back home](../README.md)

