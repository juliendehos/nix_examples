with import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/1bc5bf4beb759e563ffc7a8a3067f10a00b45a7d.tar.gz") {};
let
  _opencv3 = opencv3.override { enableOpenblas = true; };
in
stdenv.mkDerivation {
  name = "cvflip";
  src = ./.;
  buildInputs = [ cmake _opencv3 pkgconfig ];
}

