# cpp_cvflip

A Nix configuration for a C++ project (simple image processing).

## build environment

- see [default.nix](default.nix)

- install and run the derivation:

    ```
    $ nix-env -f . -i cvflip
    $ cvflip knipp3.jpg knipp3_flipped.jpg 
    ```

- or build and run locally:

    ```
    $ nix-build
    $ ./result/bin/cvflip knipp3.jpg knipp3_flipped.jpg 
    ```

## shell environment

- see [shell.nix](shell.nix)

- launch a nix environment:

    ```
    $ nix-shell
    ```

- use hooks to compile the project:

    ```
    $ my_cmake
    $ my_make
    $ cvflip knipp3.jpg knipp3_flipped.jpg 
    ```

[Back home](../README.md)

