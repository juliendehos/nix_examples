with import<nixpkgs> {};
stdenv.mkDerivation rec {
    name = "cvflip";
    src = ./.;
    buildInputs = [ cmake opencv3 pkgconfig ];
    shellHooks = ''
        export MY_SRC=`pwd`
        export MY_BUILD="$MY_SRC/build_nix"
        export MY_PATH="$HOME/opt/$name"
        export PATH="$MY_PATH/bin:$PATH"
        mkdir -p $MY_PATH
        alias my_cmake="rm -rf $MY_BUILD;\
            mkdir $MY_BUILD; cd $MY_BUILD;\
            cmake -DCMAKE_INSTALL_PREFIX=$MY_PATH ..;\
            cd -"
        alias my_make="make -C $MY_BUILD -j8 install"
    '';
}

