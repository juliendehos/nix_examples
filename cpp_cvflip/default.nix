with import<nixpkgs> {};
stdenv.mkDerivation {
    name = "cvflip";
    src = ./.;
    buildInputs = [ cmake opencv3 pkgconfig ];
}

