#include <opencv2/opencv.hpp>
#include <iostream>

using namespace std;

int main(int argc, char ** argv) {

    if (argc != 3) {
        cout << "usage: " << argv[0] << " <input file> <output file>\n";
        exit(-1);
    }
    const string infile = argv[1];
    const string outfile = argv[2];

    cv::Mat img1 = cv::imread(infile, cv::IMREAD_ANYDEPTH | cv::IMREAD_ANYCOLOR);
    if (not img1.data) {
        cout << "error: unable to open " << infile << endl;
        exit(-1);
    }

    cv::Mat img2;
    cv::flip(img1, img2, 0);
    cv::imwrite(outfile, img2);

    return 0;
}

