# nix-build docker-hello.nix
# docker load < result
# docker run -p 8080:8080 -it docker-hello

{ pkgs ? import <nixpkgs> {} }:

pkgs.dockerTools.buildImage {
  name = "docker-hello";
  contents = import ./default.nix {};

  config = { 
    Cmd = [ "/bin/docker-hello" ]; 
    ExposedPorts = {
      "8080/tcp" = {};
    };
  };
}

