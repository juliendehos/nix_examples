{-# LANGUAGE OverloadedStrings #-}

import Lucid
import Network.Wai.Middleware.Cors (simpleCors)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (middleware, scotty, get, html)

main :: IO ()
main = scotty 8080 $ do
  middleware logStdoutDev
  middleware simpleCors

  get "/" $ do
    html $ renderText $ do
      doctype_
      html_ $ body_ $ do
        h1_ "docker hello"

