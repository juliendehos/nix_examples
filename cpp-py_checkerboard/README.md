# cpp-py_checkerboard

A Nix configuration for a C++/Python project, showing how to make:

- a C++ shared lib with some dependencies (OpenCV) using Cmake and Pkg-config
- a Python binding of this lib using Boost Python and Setuptools
- a Nix packaging of the lib and binding

## build & run (the classic way)

```
mkdir checkerboard/build
cd checkerboard/build
cmake ..
make
sudo make install
cd ../..
virtualenv2 -p /usr/bin/python2 --no-site-packages ~/venv_checkerboard
source ~/venv_checkerboard/bin/activate
pip install ./pycheckerboard --upgrade
python -c "import pycheckerboard.test1 as pt; pt.test1()"
deactivate
```

## build & run (the nix way)

```
cd pycheckerboard
nix-shell
python -c "import pycheckerboard.test1 as pt; pt.test1()"
```

## output

![](screenshot_checkerboard.png)

[Back home](../README.md)

