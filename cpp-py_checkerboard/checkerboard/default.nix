{ system ? builtins.currentSystem }:

let
    pkgs = import <nixpkgs> { inherit system; };
in

with pkgs; 

stdenv.mkDerivation rec {
    opencv3gtk = import ./opencv3gtk.nix { inherit (pkgs) opencv3; };
    checkerboard = import ./checkerboard.nix { 
        inherit opencv3gtk;
        inherit (pkgs) cmake pkgconfig stdenv;
    };
}

