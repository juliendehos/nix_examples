{ cmake, opencv3gtk, pkgconfig, stdenv }:

stdenv.mkDerivation {
    name = "checkerboard";
    src = ./.;
    buildInputs = [ cmake opencv3gtk pkgconfig ];
}

