{ opencv3 }:

let
    opencv3gtk = (opencv3.override { 
        enableGtk2 = true; 
        enableOpenblas = true; 
    });

in 

opencv3gtk.overrideDerivation (
    attrs: { cmakeFlags = [attrs.cmakeFlags "-DENABLE_PRECOMPILED_HEADERS=OFF"]; }
)

