cmake_minimum_required( VERSION 3.0 )
project( checkerboard )

find_package( PkgConfig REQUIRED )
pkg_check_modules( MYPKG REQUIRED opencv )
include_directories( ${MYPKG_INCLUDE_DIRS} )

add_library( checkerboard SHARED src/checkerboard.cpp ) 
target_link_libraries( checkerboard ${MYPKG_LIBRARIES} )
install( TARGETS checkerboard DESTINATION lib )
install( FILES src/checkerboard.hpp DESTINATION "include" )

add_executable( test_checkerboard src/test_checkerboard.cpp )
target_link_libraries( test_checkerboard checkerboard ${MYPKG_LIBRARIES} )
install( TARGETS test_checkerboard DESTINATION bin )

