{ checkerboard, pythonPackages, opencv3gtk }:

pythonPackages.buildPythonPackage {
    name = "pycheckerboard";
    src = ./.;
    buildInputs = [ checkerboard pythonPackages.boost opencv3gtk ];
}

