{ system ? builtins.currentSystem }:

let
    pkgs = import <nixpkgs> { inherit system; };

    opencv3gtk = import ../checkerboard/opencv3gtk.nix { inherit (pkgs) opencv3; };

    checkerboard = import ../checkerboard/checkerboard.nix { 
        inherit opencv3gtk;
        inherit (pkgs) cmake pkgconfig stdenv; 
    };
in

with pkgs;

pythonPackages.buildPythonPackage {
    name = "pycheckerboard";
    src = ./.;
    buildInputs = [ checkerboard python27Packages.boost opencv3gtk ];
}

