{ system ? builtins.currentSystem }:

let
    pkgs = import <nixpkgs> { inherit system; };
    _pyPkgs = pkgs.pythonPackages;
in

with pkgs; _pyPkgs.buildPythonPackage {
    name = "pycheckerboard";
    src = ./.;
    buildInputs = [ _pyPkgs.boost boost opencv3 ];
}

