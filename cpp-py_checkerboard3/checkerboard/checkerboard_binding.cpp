#include "checkerboard.hpp"
#include <boost/python.hpp>
using namespace boost::python;
BOOST_PYTHON_MODULE( checkerboard ) {
    class_<cv::Mat>("Mat");
    def("create_checkerboard", &checkerboard::create_checkerboard);
    def("write_img", &checkerboard::write_img);
}
