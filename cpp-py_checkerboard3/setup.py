from setuptools import setup, Extension

checkerboard_module = Extension('checkerboard',
    sources = [
        'checkerboard/checkerboard.cpp',
        'checkerboard/checkerboard_binding.cpp'],
    libraries = [
        'boost_python', 
        'opencv_core', 
        'opencv_highgui'])

setup(name = 'pycheckerboard',
    version = '0.1',
    packages = ['pycheckerboard'],
    python_requires = '<3',
    ext_modules = [checkerboard_module])

