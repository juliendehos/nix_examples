#include "checkerboard.hpp"
#include <pybind11/pybind11.h>

PYBIND11_MODULE(checkerboard, m) {
    m.doc() = "my checkerboard module";

    m.def("create_checkerboard", &checkerboard::create_checkerboard);

    m.def("write_img", &checkerboard::write_img);

    pybind11::class_<cv::Mat>(m, "Mat");
}

