{ pkgs ? import <nixpkgs> {} }:

with pkgs;

let 
  _pybind11 = callPackage ./pybind11.nix { 
    inherit stdenv fetchFromGitHub cmake;
    python = python3;
  };
in 

python3Packages.buildPythonPackage {
  name = "pycheckerboard";
  src = ./.;
  buildInputs = [ _pybind11 opencv3 ];
}

