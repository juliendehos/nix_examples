# cpp-py_checkerboard

A Nix configuration for a C++/Python project, showing how to make:

- a C++ shared lib with some dependencies (OpenCV) using Cmake and Pkg-config
- a Python binding of this lib using Boost Python and Setuptools
- a Nix packaging of the lib and binding

## build & run 

```
nix-shell --run 'python -c "import pycheckerboard.test1 as pt; pt.test1()"'
eog out_py.png
```

