#include <iostream>
#include <armadillo>

using namespace std;
using namespace arma;

int main() {

  arma::arma_version ver;
  cout << "ARMA version: "<< ver.as_string() << endl;

  mat A = randu<mat>(4,5);
  mat B = randu<mat>(4,5);
  cout << A*B.t() << endl;

  return 0;
}

