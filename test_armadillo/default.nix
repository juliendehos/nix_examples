{ pkgs ? import <nixpkgs> {} }:

with pkgs; stdenv.mkDerivation {
    name = "test_armadillo";
    src = ./.;
    buildInputs = [ armadillo ];

    buildPhase = ''
      g++ -o test_armadillo test_armadillo.cpp -O2 -larmadillo
    '';

    installPhase = ''
      mkdir -p $out/bin
      cp test_armadillo $out/bin
    '';
}

