
# review a PR for updating armadillo

- update repo :

```
git fetch upstream pull/<id>/head:<branch>
git checkout <branch>
```

- build test_armadillo using PR :

```
nix-build -I nixpkgs=/path/to/nixpkgs/repo/
./result/bin/test_armadillo
```

