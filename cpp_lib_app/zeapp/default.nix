{ pkgs ?  import <nixpkgs> {} } :

let
  _zelib = pkgs.callPackage ../zelib/default.nix {};
in

with pkgs; stdenv.mkDerivation {
    name = "zeapp";
    src = ./.;
    buildInputs = [
      cmake
      _zelib
    ];
}

