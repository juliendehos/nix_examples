cmake_minimum_required( VERSION 2.8 )

project( zelib )

include( GNUInstallDirs )

include_directories( include )
add_library( zelib SHARED src/zelib.cpp )
set_target_properties( zelib PROPERTIES PUBLIC_HEADER "include/zelib.hpp" )
install(
    TARGETS zelib 
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
    )

add_executable( test.out src/test.cpp )
target_link_libraries( test.out zelib )

