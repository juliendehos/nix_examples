{ pkgs ?  import <nixpkgs> {} } :

with pkgs; stdenv.mkDerivation {
    name = "zelib";
    src = ./.;
    buildInputs = [
      cmake
    ];
}

