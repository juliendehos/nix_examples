## cpp_lib_app

develop a lib and an app using nix


## develop zeapp

```
cd zeapp
nix-shell
mkdir b
cd b
cmake ..
make
./zeapp
```


## just build zeapp

```
cd zeapp
nix-build
./result/bin/zeapp
```


## develop zelib

```
cd zelib
nix-shell
mkdir b
cd b
cmake ..
make
./test.out
```


## just build zelib

```
cd zelib
nix-build
tree result
```

