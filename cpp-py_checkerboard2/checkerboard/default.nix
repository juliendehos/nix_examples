{ system ? builtins.currentSystem }:

let
    pkgs = import <nixpkgs> { inherit system; };
in

with pkgs; 

stdenv.mkDerivation {
    name = "checkerboard";
    src = ./.;
    buildInputs = [ cmake opencv3 pkgconfig ];
}

