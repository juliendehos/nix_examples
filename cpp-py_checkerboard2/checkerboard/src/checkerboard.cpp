#include "checkerboard.hpp"
#include <opencv2/opencv.hpp>
using namespace cv;

Mat checkerboard::create_checkerboard(int width, int height, int size) {
    Mat img(height, width, CV_8UC3);
    const Vec3b BLACK(0, 0, 0);
    const Vec3b WHITE(255, 255, 255);
    for (unsigned i=0; i<height; i++) {
        for (unsigned j=0; j<width; j++) {
            const unsigned EVEN_I = (i/size) % 2;
            const unsigned EVEN_J = (j/size) % 2;
            img.at<Vec3b>(i, j) = EVEN_I == EVEN_J ? WHITE : BLACK;
        }
    } 
    return img;
}

void checkerboard::write_img(const std::string & filename, const Mat & img) {
  imwrite(filename, img);
}

