#ifndef CHECKERBOARD_HPP_
#define CHECKERBOARD_HPP_
#include <opencv2/opencv.hpp>
#include <string>
namespace checkerboard {
    cv::Mat create_checkerboard(int width, int height, int size);
    void write_img(const std::string & filename, const cv::Mat & img);
}
#endif

