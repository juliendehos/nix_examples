{ system ? builtins.currentSystem }:

let
    pkgs = import <nixpkgs> { inherit system; };
    checkerboard = import ../checkerboard/default.nix { inherit system; };
in

with pkgs; pythonPackages.buildPythonPackage {
    name = "pycheckerboard";
    src = ./.;
    buildInputs = [ checkerboard python27Packages.boost opencv3 ];
}

