with import <nixpkgs> {};

let
    _ghc = haskellPackages.ghcWithPackages (pkgs: with pkgs; [
        split
    ]);
in

pkgs.stdenv.mkDerivation {
    name = "tictactoe";
    src = ./.;
    buildInputs = [ _ghc ];
    buildPhase = "ghc tictactoe.hs";
    installPhase = ''
        mkdir -p $out/bin
        cp tictactoe $out/bin/
    '';

}

