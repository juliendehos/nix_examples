with import <nixpkgs> {};
stdenv.mkDerivation {
    name = "tictactoe-env";
    buildInputs = [
        (haskellPackages.ghcWithPackages (pkgs: with pkgs; [ 
            split
        ]))
    ];
}

