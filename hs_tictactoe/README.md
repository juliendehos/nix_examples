
# hs_tictactoe

A Nix configuration for a Haskell project (a simple tictactoe game with no AI).

## build environment

- see [default.nix](default.nix)

- build project :

    ```
    $ nix-build
    ```

- run project :

    ```
    $ ./result/bin/tictactoe
    ```

## shell environment

- see [shell.nix](shell.nix)

- launch a nix environment:

    ```
    $ nix-shell
    ```

- launch ghc commands as usual:

    ```
    $ runghc tictactoe.hs
    ```

[Back home](../README.md)

