#!/usr/bin/env python3

import numpy as np

x = np.random.rand(3)

print("the mean value of {} is {}".format(x, np.mean(x)))

