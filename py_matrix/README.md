# py_matrix

A Nix configuration for a simple python script (print a random matrix).

## shell environment

- see [shell.nix](shell.nix)

- launch a nix environment and run the script:

    ```
    $ nix-shell 

    [nix-shell]$ python matrix.py 
    the mean value of [ 0.87285935  0.73873141  0.2757265 ] is 0.6291057542807504
    ```

[Back home](../README.md)

